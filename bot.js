const Discord = require('discord.js')
const fs = require('fs')
const child_process = require('child_process')

TOKEN = require('./token.js').TOKEN

const ADMIN_ROLES = new Set([
	'395432980698497045', //@techfairy
	'416526149129076737', //@fbi
	'395596462354071552'  //@allMOD
])

const discordBot = new Discord.Client()

function isAdmin(msg) {
  for (const guild of discordBot.guilds.array()) {
    const mymember = guild.member(msg.author)
    if (mymember) {
	for (const role of mymember.roles.array()){
		if (ADMIN_ROLES.has(role.id)){
			return true
		}
        }
    }
  }
  return false
}

// Write your commands below
const commands = new Map()

const blockips_path = '/etc/nginx/blockips.conf'

function toIP(input){
  if (input.match(/^[0-9a-z][0-9a-z.\:]*[0-9a-z]$/)){
	return input
  }
  return null
}

commands.set('.allow', body => {
  const IPtoBan = toIP(body)
  if (IPtoBan){
    child_process.execSync('ex \'+g/^deny ' + IPtoBan + ';$/d\' -cwq ' + blockips_path + ' && service nginx reload')
    return IPtoBan + " is no longer banned."
  } else {
    return "Invalid IP."
  }
})

commands.set('.deny', body => {
  const IPtoBan = toIP(body)
  if (IPtoBan){
    fs.appendFileSync(blockips_path, 'deny ' + IPtoBan + ';\n')
    child_process.execSync('service nginx reload')
    return IPtoBan + " is now banned."
  } else {
    return "Invalid IP."
  }
})

logsdir='/var/log/nginx/manual_raid_submit_logs'

commands.set('.leaderboardtoday', body => {
  return child_process.execSync('./leaderboard.sh',options={cwd : logsdir}).toString()
})

commands.set('.leaderboard10day', body => {
  return child_process.execSync('./leaderboard.sh 10 | head -n20',options={cwd : logsdir}).toString()
})

commands.set('.leaderboardalltime', body => {
  return child_process.execSync('./leaderboard.sh 9999 | head -n20',options={cwd : logsdir}).toString()
})

commands.set('.leaderboardyesterday', body => {
  return child_process.execSync('./yesterleaderboard.sh',options={cwd : logsdir}).toString()
})

commands.set('.submitlog', body => {
  return child_process.execSync('./submitlog.sh',options={cwd : logsdir}).toString()
})

commands.set('.submitlognew', body => {
  return child_process.execSync('./new_ips.sh',options={cwd : logsdir}).toString()
})

commands.set('.banlist', body => {
  //return fs.readFileSync(blockips_path).toString()
  return "Banned IPs:\n" + child_process.execSync('cat ' + blockips_path + ' | tr -d \';\' | cut -d\' \' -f2 ').toString()
})

function handleMessage(msg, channel) {
  const body = msg.content.split(' ')
  if (!body.length) return null

  const [cmd] = body

  //Allow leaderboard commands on leaderboard channel without admin check
  if (cmd.startsWith(".leaderboard") && (msg.channel.name === 'leaderboard' || msg.channel.type === 'dm')){
    if (commands.has(cmd)) {
	    const result = commands.get(cmd)(body.slice(1).join(' '))
	    if (result) {
	      // discord has a max message length of 1500, we'll just truncate
	      channel.send(result.substring(0, 1500))
	    }
    }
  } else {
    if (commands.has(cmd) && (msg.channel.name === 'fairycrowdbot_commands' || msg.channel.type === 'dm')) {
  	if (!isAdmin(msg)) return null

	    const result = commands.get(cmd)(body.slice(1).join(' '))
	    if (result) {
	      // discord has a max message length of 1500, we'll just truncate
	      channel.send(result.substring(0, 1500))
	    }
    }
  }
}

discordBot.on('message', msg => {
   handleMessage(msg, msg.channel)
})

discordBot.on('ready', function() {
    discordBot.user.setUsername("The Ban Hammer");
})

discordBot.login(TOKEN)
